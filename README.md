# Rusty Buckets
An interpreter for the esolang **Buckets** written in about 2.5 hours using **Rust**.

## Syntax
t = -1

d = +1

s = sets all buckets to 0

f = sets all buckets to 15

o = print state of buckets, end program

## Buckets?
Buckets is an esoteric programming language.

The syntax is made up of 5 simple commands, all other characters are ignored by this interpreter.

You can only manipulate the first bucket by using t or d.

No buckets can be over 15.

If you add to a bucket that is 15, it overflows into the next bucket, and this recurses.

If you subtract from a bucket that is 0, it will grab one item from the bucket next to it, this also recurses.
