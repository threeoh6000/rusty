use std::env;
use std::fs;
use std::io;
use std::process;

fn get(raw: Option<usize>) -> usize {
	match raw {
		Some(x) => x,
		None => 0,
	}

}

fn main() -> io::Result<()> {

	let mut buckets = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	let args: Vec<String> = env::args().collect();
	let program = fs::read_to_string(&args[1])?;
	let mut i = 0;

	while i < program.len() {
		let counter: u8 = program.as_bytes()[i];
		if counter == 116 {
			buckets[0] -= 1;
			while buckets.contains(&-1) {
				let raw = buckets.iter().position(|&x| x == -1);
				let index = get(raw);
				let intwo: usize = &index + 1;
				buckets[index] = 0;
				if buckets[intwo] > 0 {
					buckets[intwo] -= 1;
				} else {
					buckets[intwo] = -1;
					buckets[index] = 0;
				}
			}
		} else if counter == 100 {
			buckets[0] += 1;
			while buckets.contains(&16) {
				let raw = buckets.iter().position(|&x| x == 16);
				let index = get(raw);
				let intwo: usize = &index + 1;
				buckets[index] = 0;
				if buckets[intwo] < 15 {
					buckets[intwo] += 1;
				} else {
					buckets[intwo] = 16;
				}
			}
		} else if counter == 115 {
			buckets = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		} else if counter == 102 {
			buckets = [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15];
		} else if counter == 111 {
			for x in buckets {
				println!("{}", x);
			}
			process::exit(0);
		} else {}
		i = i + 1;
	}

	Ok(())

}
